import { Component } from '@angular/core';

@Component({
  selector: 'app-progress-bar',
  templateUrl: './progress-bar.component.html',
  styleUrls: ['./progress-bar.component.css']
})

export class ProgressBarComponent {
  tab = [
    {
      title: 'Initialisation du test technique', value: 50
    },
    {
      title: 'Avancement de la phase de développement', value: 25
    },

  ]

    ;


remiseAzero(){
this.tab.forEach(element => {
  element.value=0; 
  
});}

AjoutFive(){
  this.tab.forEach(element => {
    if(element.value<100){
      element.value+=5; 
    }
  
    
  });
  
  }

  Ajoutten(){
    this.tab.forEach(element => {
      if(element.value<100){
      element.value+=10; 
      }
    });
    
    }
}
